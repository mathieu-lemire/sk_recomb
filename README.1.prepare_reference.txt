This guide is illustrated by using the 1000 Genomes Project reference data prepared by Beagle authors.

Download phased data from 

http://bochet.gcc.biostat.washington.edu/beagle/1000_Genomes_phase3_v5a/b37.vcf

Download sample info from

http://bochet.gcc.biostat.washington.edu/beagle/1000_Genomes_phase3_v5a/sample_info/integrated_call_samples_v3.20130502.ALL.panel

Download genetic map 

http://compgen.rutgers.edu/downloads/rutgers_map_v3a.zip

###

Extract haplotypes and create plink files containing them. In the fam files, one ID represents one haplotype. 

for chr in {1..22}; do 
 make_hap.sh $chr 
done 


###

Unzip genetic maps 

unzip rutgers_map_v3a.zip 

cd rutgers_map_v3a

# only extracting required fields.
# writing the first instance of rounded down genetic maps (one decimal point)

for c in {1..22}; do 
 echo $c 
 sed 's/ /,/g' RUMapv3a_B137_chr$c.txt |\
  awk 'BEGIN{FS=","; last=-9}{mapround=int(10*$7)}\
       {if( mapround != last && NR>1 ){ print $6,$7; last=mapround } }'  > physical_genetic_map_ROUNDED_chr$c.txt 
 done 

