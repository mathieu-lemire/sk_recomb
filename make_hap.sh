#!/bin/bash 

if [ -z $chr ]; then 
 chr=$1
fi 




mkdir $chr
cd $chr


module load bcftools
module load vcftools 

# header 

bcftools view  -h chr${chr}.1kg.phase3.v5a.vcf.gz  | grep -v CHROM > _hap.vcf
bcftools view  -h chr${chr}.1kg.phase3.v5a.vcf.gz  | grep CHROM |\
  awk '{printf $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9}{for(i=10; i<=NF; i++){printf "\t"$i"_1\t"$i"_2"}}{printf "\n"}'  >> _hap.vcf

# doubling the individual alleles into genotypes .  only using biallelic SNPs (NO indels) 
bcftools view -H -m2 -M2 -v snps chr${chr}.1kg.phase3.v5a.vcf.gz |  sed 's/\([0-9]\)|\([0-9]\)/\1|\1\t\2|\2/g'  >> _hap.vcf

##

vcftools --vcf _hap.vcf --plink-tped --out hap

## tfam  
bcftools view  -h chr${chr}.1kg.phase3.v5a.vcf.gz  | grep CHROM |\
  awk '{printf $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9}{for(i=10; i<=NF; i++){printf "\t"$i"_1\t"$i"_2"}}{printf "\n"}' |\
 cut -f 10- | transpose | awk '{print $1,$1,"0 0 0 0"}' | sed 's/_[12]//'  > hap.tfam

\plink --memory 12000 --tped hap.tped --tfam hap.tfam --make-bed --out hap${chr} 

\rm _hap* 
\rm hap.tped 
\rm hap.tfam 

cd ..
