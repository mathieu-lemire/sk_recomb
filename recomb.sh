#!/bin/bash 

module load bcftools


# THIS NEEDS TO BE DEFINED 
# genetic map: directory containing the files physical_genetic_map_ROUNDED_chr$c.txt
mapdir=

if [ -z $prefix ]; then 
 # real data upon which sim is based on
 prefix=$1
fi 

if [ -z $chr ]; then 
 chr=$2 
fi 

# size of chr1
mxsize=`tail ${mapdir}/physical_genetic_map_ROUNDED_chr1.txt  | sort -k 1,1 -g -r | head -1 | awk '{print $2}'`

mapfile=${mapdir}/physical_genetic_map_ROUNDED_chr$chr.txt

# selecting that many haplotypes out of all, which will be "recombined".
# the final output will consist of samplesubsetsize/2 simulated individuals.
samplesubsetsize=400 
# to speed up, once a breakpoint is determined, that many haplo (in pairs) are 
# recombined at the same position (think hotspots). samplesubsetsize must be a multiple of this.
sampleblocksize=10


maxrecomb=42 

echo SAMPLESUBSETSIZE $samplesubsetsize

# for chr1, there are maxrecomb recombinations events, rest is proportional to size relative to chr1.  rounded up 
nrecomb=`sort -k 2,2 -g -r $mapfile | head -1 | awk '{print  int( '$maxrecomb'* $2/'$mxsize' + .99 ) }'`

echo CHROMOSOME $chr 
echo SIMULATING $nrecomb RECOMBINATION EVENTS MAXRECOMB=${maxrecomb}
echo SAMPLEBLOCKSIZE $sampleblocksize

mkdir _chr${chr}
cd _chr${chr}

shuf ${prefix}.fam  | head -n $samplesubsetsize | awk '{print $1,$2}' > $$_keep
\plink --memory 8000 --bfile $prefix --keep $$_keep --chr $chr --out chr${chr} --make-bed  > /dev/null 
awk '{$1=NR}{$2=NR}{print}' chr${chr}.fam > _fam ; mv _fam chr${chr}.fam 

for i in {1..$nrecomb}; do 

shuf chr${chr}.fam | awk '{print $1,$2}'  > $$_random_order.${i}

nsa=`wc -l $$_random_order.${i} | awk '{print $1}'`
from=`expr 1 - $sampleblocksize`
to=0

\rm $$_merge_list 
while [ $to -lt $nsa ]; do 
 from=`expr $from + $sampleblocksize`
 to=`expr $to + $sampleblocksize`
 echo $i $from $to $nsa 
 if [ $to -gt $nsa ]; then 
   to=$nsa
 fi
 awk 'NR>='$from' && NR<='$to' {print $1,$2}' $$_random_order.${i} > $$_keep 

 \plink --memory 8000 --bfile chr${chr} --keep $$_keep --make-bed  --out subset   > /dev/null 

 # including one recombination
 nsnp=`wc -l subset.bim | awk '{print $1}'`

 # the mapfile is approx evenly spaced wrt genetic map, so I can just use shuf to pick a point 
 # of recomb 
 rec=`cat $mapfile | shuf | head -1 | awk '{print $1}'`
 echo RECOMB AT $rec 

 awk '$4<'$rec' {print $2}' subset.bim > $$_extract1 
 awk '$4>='$rec' {print $2}' subset.bim > $$_extract2

 \plink --memory 8000 --bfile subset --extract $$_extract1 --make-bed --out $$_left  > /dev/null 
 \plink --memory 8000 --bfile subset --extract $$_extract2 --make-bed --out $$_right > /dev/null
 
 # do I recombine left or right?
 # If I don't do that then between replicates, samples with the same name 
 # will look more related because they share the start of the chromosomes,
 # but that's by construction only
 # the continue statement checks that the sample on left and right is not 
 # the same (all samples are recombined with another and not with itself
 doright=`seq 1 2 | shuf | head -1`
 if [ $doright -eq 1 ]; then 

  nfam=`wc -l $$_right.fam | awk '{print $1}'`
  # to make sure no one is recombining with itself 
  continue=1
  while [ $continue -gt 0 ]; do 
   seq 1 $nfam  | shuf > _reorder
   continue=`awk '$1==NR {print}' _reorder |wc -l | awk '{print $1}'`
  done 
  paste _reorder $$_right.fam | sort -k 1,1 -g | cut -f 2- > $$_right_${i}_${to}.fam  
  \cp $$_right_${i}_${to}.fam  $$_right.fam 

 else

  nfam=`wc -l $$_left.fam | awk '{print $1}'`
  # to make sure no one is recombining with itself
  continue=1
  while [ $continue -gt 0 ]; do
   seq 1 $nfam  | shuf > _reorder
   continue=`awk '$1==NR {print}' _reorder |wc -l | awk '{print $1}'`
  done
  paste _reorder $$_left.fam | sort -k 1,1 -g | cut -f 2- > $$_left_${i}_${to}.fam
  \cp $$_left_${i}_${to}.fam  $$_left.fam

 fi


 \plink --memory 8000 --bfile $$_left --bmerge $$_right.bed $$_right.bim $$_right.fam --make-bed --out recomb_${to} > /dev/null 
 echo recomb_${to}.bed recomb_${to}.bim recomb_${to}.fam >> $$_merge_list 

done   
# above done is for while [ $to -lt $nsa ]; do



echo $i merging...
\plink --memory 8000 --bfile recomb_${sampleblocksize} --merge-list $$_merge_list --make-bed --out chr$chr > /dev/null 

\rm recomb_* 

done 
# above done is for for i in {1..$nrecomb}; do


\rm $$_* 
\rm subset* 


# recombining haplotypes to form diplotypes

\plink  --memory 8000 --bfile chr$chr  --recode vcf --out chr$chr

bcftools view -h chr${chr}.vcf | grep -v CHROM > _tmp.vcf
bcftools view -h chr${chr}.vcf | grep  CHROM |\
 awk '{for( i=1; i<=9; i++){printf $i"\t"}}{for(i=10; i<=NF;i=i+2){printf $i"\t"}}{printf "\n"}'  >> _tmp.vcf
bcftools view -H chr${chr}.vcf | sed 's/\/[01]\t[01]\//\//g'   >> _tmp.vcf

plink --vcf _tmp.vcf --make-bed --out chr$chr

\rm *~
\rm _tmp.vcf









