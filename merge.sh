#!/bin/bash 

rep=`pwd | rev | cut -f 1 -d'/' |rev`

ls -1 _chr*/chr*.bed > _1 
ls -1 _chr*/chr*.bim > _2
ls -1 _chr*/chr*.fam > _3

paste _1 _2 _3 > _merge_list 
\plink --memory 8000 --bfile _chr1/chr1 --merge-list _merge_list --make-bed --out $rep 

num=`echo $rep | sed 's/repl//g'`

awk '{$1="'$num'"}{print}' $rep.fam > _f; mv _f $rep.fam 



\rm -rf _chr* 
\rm _[0-9]
\rm _merge_list 




