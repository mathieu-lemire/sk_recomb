# preparing files, these are the "founders" haplotypes 

# location of the beagle 1000 Genomes project vcf files and sample info 
# (assumed to be in same directory)
beagleref=

kgsamples=$beagleref/integrated_call_samples_v3.20130502.ALL.panel

# directory where make_hap.sh was executed. assumed to to be same as beagleref 
haplodir=$beagleref 

# Focus on EUR samples for illustration purposes 
# Creating plink files containing haplotypes for EUR samples and SNPs with maf>0.01 
grep EUR $kgsamples | awk '{print $1}' | sort -k 1,1 > _get 

sort -k 1,1 $haplodir/1/hap1.fam | awk '{print $1,$2}' > _fam
join -1 1 -2 1 _get _fam > _keep 

\rm _merge_list 
for c in {1..22}; do 
 if [ -f $haplodir/$c/hap$c.fam ]; then 
   plink --memory 12000 --bfile $haplodir/$c/hap$c --keep _keep --make-bed --out _tmp$c --threads 1 --maf 0.01 
   echo _tmp${c}.bed _tmp${c}.bim _tmp${c}.fam >> _merge_list 
 fi
done 

plink --memory 12000  --bfile _tmp1 --merge-list _merge_list --make-bed --out kgeur_hap --threads 1 

# dealing with missnp 
for c in {1..22}; do
    plink --memory 12000 --bfile _tmp$c  --exclude kgeur_hap-merge.missnp  --make-bed --out _tmp$c --threads 1
done 

plink --memory 12000  --bfile _tmp1 --merge-list _merge_list --make-bed --out kgeur_hap --threads 1

rm _tmp*
rm _fam 
rm _keep 


#######################

prefix=$PWD/kgeur_hap 

# will simulate 10 replicates of 200 simulated samples (number set in recomb)
for r in {101..110}; do 
  mkdir repl${r}
  cd repl${r}
  qrecomb.sh $prefix
  cd ..
done  

########################

# Once all is done 

ls -1 repl*/repl*[0-9].bed > _bed 
ls -1 repl*/repl*[0-9].bim > _bim 
ls -1 repl*/repl*[0-9].fam > _fam

paste _bed _bim _fam > _merge_list

plink --bfile repl101/repl101 --merge-list _merge_list --make-bed --out replicates 

rm _bed _bim _fam _merge_list

# the repl* directories can now be deleted






