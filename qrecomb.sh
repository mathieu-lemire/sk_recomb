#!/bin/bash 


# where does recomb.sh lives
scriptdir=


rep=`pwd | rev | cut -f 1 -d'/' |rev`

echo $rep 

prefix=$1 

joblist=

for chr in `seq 1 22`; do
 job=$(qsub -d . -N _${rep}_${chr} -l vmem=10G,mem=10G,walltime=24:00:00 -v chr=$chr,prefix=$prefix $scriptdir/recomb.sh)
 joblist=`echo ${joblist}:${job}`
done 

qsub -d . -N _merge_${rep} -W depend=afterok${joblist} -l vmem=10G,mem=10G $scriptdir/merge.sh 



